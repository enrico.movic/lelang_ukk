<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Siswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->foreignId('NISN');
            $table->integer('NIS')->unique();
            $table->string('nama');
            $table->integer('id_kelas')->unique()->nullable();
            $table->string('alamat');
            $table->string('no_telp');
            $table->integer('id_spp')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
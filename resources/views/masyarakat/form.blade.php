@extends('layouts.app')

@section('page-name', (isset($masyarakat) ? 'Ubah Barang' : 'Barang Baru'))

@section('content')
<div class="row">
    <div class="col-8">
        <form action="{{ (isset($masyarakat) ? route('masyarakat.update', $siswa->id_user) : route('masyarakat.create')) }}" method="post" class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Masyarakat</h3>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{ $error }}<br>
                    @endforeach
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">NAMA LENGKAP</label>
                            <input type="text" class="form-control" name="nama_lengkap" placeholder="nama_lengkap" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">USERNAME</label>
                            <input type="text" class="form-control" name="username" placeholder="username" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">PASSWORD</label>
                            <input type="text" class="form-control" name="password" placeholder="password" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">TELEPON</label>
                            <input type="number" class="form-control" name="telp" placeholder="telp" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div class="d-flex">
                    <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                    <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
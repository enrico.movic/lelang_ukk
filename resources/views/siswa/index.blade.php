@extends('layouts.app')

@section('page-name','Barang')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            @yield('page-name')
        </h1>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@yield('page-name')</h3>
                    <a href="{{route('siswa.create') }}" class="btn btn-outline-primary btn-sm ml-5">Tambah Barang</a>
                    <div class="card-options">
                    </div>
                </div>
                @if(session()->has('msg'))
                <div class="card-alert alert alert-{{ session()->get('type') }}" id="message" style="border-radius: 0px !important">
                    @if(session()->get('type') == 'success')
                        <i class="fe fe-check mr-2" aria-hidden="true"></i>
                    @else
                        <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> 
                    @endif
                        {{ session()->get('msg') }}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-nowrap">
                        <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>ID BARANG</th>
                            <th>NAMA BARANG</th>
                            <th>TANGGAL MASUK BARANG</th>
                            <th>HARGA AWAL</th>
                            <th>DESKRIPSI BARANG</th>
                            <th> </th> 
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $index => $item)
                            <tr>
                                <td><span class="text-muted">{{ $index+1 }}</span></td>
                                <td>{{$item->id_barang}}</td>
                                <td>
                                    {{$item->nama_barang}}
                                </td>
                                <td>
                                    {{$item->tgl}}
                                </td>
                                <td>
                                    {{$item->harga_awal}}
                                </td>
                                <td>
                                    {{$item->deskripsi_barang}}
                                </td>
                                <td class="text-center">
                                    <form action="{{ route('siswa.destroy', $item->id_barang) }}" method="POST" id="form-{{ $item->id_barang }}">
                                        @method('DELETE')
                                        @csrf 
                                    <a class="icon btn-delete" href="#!" data-id="{{ $item->id_barang }}" title="delete item">
                                        <i class="fe fe-trash"></i>
                                    </a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    require(['jquery', 'sweetalert'], function ($, sweetalert) {
        $(document).ready(function () {

            $(document).on('click','.btn-delete', function(){
                formid = $(this).attr('data-id');
                swal({
                    title: 'Are you sure to delete?',
                    text: 'items that have been deleted cannot be returned',
                    dangerMode: true,
                    buttons: {
                        cancel: true,
                        confirm: true,
                    },
                }).then((result) => {
                    if (result) {
                        $('#form-' + formid).submit();
                    }
                })
            })

        });
    });
</script>
@endsection


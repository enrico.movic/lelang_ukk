@extends('layouts.app')

@section('page-name','Siswa')

@section('content')
                        @foreach($siswa as $item)
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@yield('page-name')</h3>
                </div>
                <div class="card-body">
                    <p><b>Kelas</b> : {{$item->nama_kelas}} </p>
                    <p>
                        <b>Nama</b> : {{$item->nama}} 
                    </p>
                    <p><b>NISN</b> : {{$item->NISN}} </p>
                    <p><b>NIS</b> : {{$item->NIS}} </p>
                    <p><b>Alamat</b> : {{$item->alamat}} </p>
                    <p><b>No Telepon</b> : {{$item->no_telp}} </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tagihan SPP</h3>
                </div>
                <div class="card-body">
                    <table class="table card-table table-hover table-vcenter">
                        <tr>
                            <th>Juli</th>
                            <th>Agustus</th>
                            <th>September</th>
                            <th>Oktober</th>
                            <th>November</th>
                            <th>Desember</th>
                            <th>Januari</th>
                            <th>Februari</th>
                            <th>Maret</th>
                            <th>April</th>
                            <th>Mei</th>
                            <th>Juni</th>
                        </tr> 
                        <tr>
                            <td>{{ $item->Transaksi->Juli}}</td>
                            <td>{{ $item->Transaksi->Agustus}}</td>
                            <td>{{ $item->Transaksi->September}}</td>
                            <td>{{ $item->Transaksi->Oktober}}</td>
                            <td>{{ $item->Transaksi->November}}</td>
                            <td>{{ $item->Transaksi->Desember}}</td>
                            <td>{{ $item->Transaksi->Januari}}</td>
                            <td>{{ $item->Transaksi->Febuari}}</td>
                            <td>{{ $item->Transaksi->Maret}}</td>
                            <td>{{ $item->Transaksi->April}}</td>
                            <td>{{ $item->Transaksi->Mei}}</td>
                            <td>{{ $item->Transaksi->Juni}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
                        @endforeach
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />
@endsection
@section('js')
<script>
    require(['jquery', 'moment','daterangepicker'], function ($, moment, daterangepicker) {
        $(document).ready(function () {
            $('input[name="dates"]').daterangepicker();
        });
    });
</script>
@endsection
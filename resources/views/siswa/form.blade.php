@extends('layouts.app')

@section('page-name', (isset($siswa) ? 'Ubah Barang' : 'Barang Baru'))

@section('content')
<div class="row">
    <div class="col-8">
        <form action="{{ (isset($siswa) ? route('siswa.update', $siswa->id_barang) : route('siswa.create')) }}" method="post" class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Barang</h3>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{ $error }}<br>
                    @endforeach
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">NAMA BARANG</label>
                            <input type="text" class="form-control" name="nama_barang" placeholder="nama_barang" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">HARGA AWAL</label>
                            <input type="number" class="form-control" name="harga_awal" placeholder="harga_awal" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">TANGGAL MASUK BARANG</label>
                            <input type="date" class="form-control" name="tgl" placeholder="deskripsi_barang" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">DESKRIPSI BARANG</label>
                            <input type="text" class="form-control" name="deskripsi_barang" placeholder="deskripsi_barang" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div class="d-flex">
                    <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                    <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
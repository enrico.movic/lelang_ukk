<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Masyarakat;

class MasyarakatController extends Controller
{
   public function index(Request $request) 
    {
       $masyarakat = Masyarakat::all();
        return view('masyarakat.index', [
            'masyarakat' => $masyarakat
        ]);
    }
    public function create()
    {
        return view('masyarakat.form');
    }
   public function store(Request $request)
    {

        $id_user        = $request->input('id_user');
        $nama_lengkap        = $request->input('nama_lengkap');
        $username                = $request->input('username');
        $password         = $request->input('password');
        $telp          = $request->input('telp');
        $current_date_time = Carbon::now()->toDateTimeString();


        $data = array(
            'id_user' => $id_user,
            'nama_lengkap' => $nama_lengkap,
            'username'   => $username,
            'password'  => $password,
            'telp' => $telp
         );

			DB::table('masyarakat')->insert($data);

            return redirect()->route('masyarakat.index')->with([
                'type' => 'success',
                'msg' => 'Barang ditambahkan'
            ]);
    }

        public function destroy(Request $request, $id_user)
    {
        Siswa::where('id_user',$id_user)->delete();
            return redirect()->route('masyarakat.index')->with([
                'type' => 'success',
                'msg' => 'Barang dihapus'
            ]);
    }

   //  public function edit(Siswa $siswa)
   //  {
   //      $kelas = Kelas::all();
   //      return view('siswa.form', [
   //          'siswa' => $siswa,
   //          'kelas' => $kelas
   //      ]);
   //  }

   //  public function update(Request $request)
   //  {

   //      $jjj = DB::table('siswa')->where('id',$request->idnya)->update([
   //      'NISN' => $request->NISN,
   //      'NIS' => $request->NIS,
   //      'nama' => $request->nama,
   //      'nama_kelas' => $request->nama_kelas,
   //      'alamat' => $request->alamat,
   //      'no_telp' => $request->no_telp
   //  ]);

   //          return redirect()->route('siswa.index')->with([
   //              'type' => 'success',
   //              'msg' => 'Kelas diubah'
   //          ]);
   //  }

   //   public function show(Request $request)
   //  {
   //  	$siswa = Siswa::all();
   //  	return view('siswa.show', [
   //  		'siswa' => $siswa
   //  	]);
   //  }

}

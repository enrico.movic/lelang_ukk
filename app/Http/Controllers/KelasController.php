<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Kelas;

class KelasController extends Controller
{
    public function index()
    {
    	$kelas = Kelas::all();
        return view('kelas.index' , [
            'kelas' => $kelas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kelas.form');
    }

    public function store(Request $request)
    {
    	// dd($request);
        $request->validate([
            'nama' => 'required|max:255',
            'kk' => 'required|max:255',
        ]);

        $nama        = $request->input('nama');
        $kk          = $request->input('kk');

        $data = array(
            'nama_kelas' => $nama,
            'kompetensi_keahlian' => $kk
         );
        
			DB::table('kelas')->insert($data);
            return redirect()->route('kelas.index');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas)
    {
        return view('kelas.form', [
            'kelas' => $kelas
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'kk' => 'required|max:255',
        ]);

        DB::table('kelas')->where('id',$request->kelas)->update([
        'nama_kelas' => $request->nama,
        'kompetensi_keahlian' => $request->kk
    ]);

            return redirect()->route('kelas.index')->with([
                'type' => 'success',
                'msg' => 'Kelas diubah'
            ]);
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function destroy(Kelas $kelas)
    {
        if($kelas->delete()){
            return redirect()->route('kelas.index')->with([
                'type' => 'success',
                'msg' => 'Kelas dihapus'
            ]);
        }else{
            return redirect()->route('kelas.index')->with([
                'type' => 'danger',
                'msg' => 'Err.., Terjadi Kesalahan'
            ]);
        }
    }
}

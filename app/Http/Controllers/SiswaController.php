<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Siswa;

class SiswaController extends Controller
{
   public function index(Request $request) 
    {
       $siswa = siswa::all();
        return view('siswa.index', [
            'siswa' => $siswa
        ]);
    }
    public function create()
    {
        return view('siswa.form');
    }
   public function store(Request $request)
    {

        $id_barang        = $request->input('id_barang');
        $nama_barang        = $request->input('nama_barang');
        $tgl                = $request->input('tgl');
        $harga_awal         = $request->input('harga_awal');
        $deskripsi_barang          = $request->input('deskripsi_barang');
        $current_date_time = Carbon::now()->toDateTimeString();


        $data = array(
            'id_barang' => $id_barang,
            'nama_barang' => $nama_barang,
            'tgl'   => $tgl,
            'harga_awal' => $harga_awal,
            'deskripsi_barang' => $deskripsi_barang
         );

			DB::table('barang')->insert($data);

            return redirect()->route('siswa.index')->with([
                'type' => 'success',
                'msg' => 'Barang ditambahkan'
            ]);
    }

        public function destroy(Request $request, $id_barang)
    {
        Siswa::where('id_barang',$id_barang)->delete();
            return redirect()->route('siswa.index')->with([
                'type' => 'success',
                'msg' => 'Barang dihapus'
            ]);
    }

   //  public function edit(Siswa $siswa)
   //  {
   //      $kelas = Kelas::all();
   //      return view('siswa.form', [
   //          'siswa' => $siswa,
   //          'kelas' => $kelas
   //      ]);
   //  }

   //  public function update(Request $request)
   //  {

   //      $jjj = DB::table('siswa')->where('id',$request->idnya)->update([
   //      'NISN' => $request->NISN,
   //      'NIS' => $request->NIS,
   //      'nama' => $request->nama,
   //      'nama_kelas' => $request->nama_kelas,
   //      'alamat' => $request->alamat,
   //      'no_telp' => $request->no_telp
   //  ]);

   //          return redirect()->route('siswa.index')->with([
   //              'type' => 'success',
   //              'msg' => 'Kelas diubah'
   //          ]);
   //  }

   //   public function show(Request $request)
   //  {
   //  	$siswa = Siswa::all();
   //  	return view('siswa.show', [
   //  		'siswa' => $siswa
   //  	]);
   //  }

}
